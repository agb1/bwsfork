### What is this repository for? ###

* BiG World Setup:

[Forum SHS](http://www.shsforums.net/topic/56670-big-world-setup-an-attempt-to-update-the-program/)

[Forum Beamdog](https://forums.beamdog.com/discussion/44476/tool-big-world-setup-bws-mod-manager-for-baldurs-gate-enhanced-edition-trilogy-for-windows#latest)

### Contribution guidelines ###

* Don't use BitBucket web-interface edit because of the bug 

### How do I contribute? ###

* learn git basics
* fork this repository using "SmartGit" or other prefered tool
* add mods/make other changes
* create pull request for this repo

### Who do I talk to? ###
[AL|EN](http://www.shsforums.net/user/10953-alien/)

[Quiet](http://www.shsforums.net/user/13265-quiet/)

[dabus](http://www.shsforums.net/user/11682-dabus/)